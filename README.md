# Ciência de Dados
## Repositório para aula

## Requisitos para a máquina hospedeira
- [Virtualbox](https://bityli.com/cNGOp)
- [Git](https://git-scm.com/book/pt-br/v2/Come%C3%A7ando-Instalando-o-Git )
- [Java 8 ou superior](https://www.oracle.com/br/java/technologies/javase/javase-jdk8-downloads.html)
- [Pentaho DI 9.1](https://www.oracle.com/br/java/technologies/javase/javase-jdk8-downloads.html)

## Documentação adicional

- Pentaho:
   - [Windows](https://www.hitachivantara.com/en-us/pdf/white-paper/pentaho-community-edition-installation-guide-for-windows-whitepaper.pdf)

   - [Linux](https://www.hitachivantara.com/en-us/pdf/white-paper/pentaho-ce-installation-guide-on-linux-operating-system-whitepaper.pdf)
     - https://www.ambientelivre.com.br/tutoriais-pentaho-bi/instalando-o-pentaho-data-integration-pdi-kettle-no-linux.html
     - https://www.ubuntupit.com/how-to-install-pentaho-data-integration-pdi-tool-on-ubuntu/
     - https://geekbi.wordpress.com/2020/09/24/pdi-8-x-9-x-no-ubuntu-20-04/



