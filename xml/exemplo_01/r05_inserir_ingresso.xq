declare variable $s_sessao  := 1;
declare variable $i_tipo  := 1;
declare variable $i_assento  := "10F";

for $d in doc("ingressos.xml")//sessao[@id=2]
  return insert node (
    <ingresso tipo="{$i_tipo}" assento="{$i_assento}" />
  )  into $d