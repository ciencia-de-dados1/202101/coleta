<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <html>
    <body>
    <h2>Ingressos</h2>
    <xsl:apply-templates/>
    </body>
    </html>
  </xsl:template>
  
  <xsl:template match="ingressos/sessao">
   <h3>
     Sessao: <xsl:apply-templates select="@id"/>
   </h3>   
  </xsl:template>

</xsl:stylesheet>