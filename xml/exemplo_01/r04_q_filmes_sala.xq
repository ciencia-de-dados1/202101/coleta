xquery version "3.0";

declare namespace output = "http://www.w3.org/2010/xslt-xquery-serialization";

declare option output:method "html";
declare option output:media-type "text/html";

declare variable $s_sala  := 1;
declare variable $sessoes := doc("sessoes.xml")//sessao[sala=$s_sala];

<html>
<meta charset="UTF-8" />
<head>
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous" />

<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous" />

<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

</head>
<body>
<div class="container-fluid">
  <div class="panel panel-default">
    <div class="panel-heading">Filmes na Sala {$s_sala} - <span class="label label-default">
    {
      let $total_filmes := count(doc("filmes.xml")//filme[@id=$sessoes//filme])
      return $total_filmes
    }
  </span></div>
  <div class="panel-body">
    <table class="table">
    {
      for $sessao in $sessoes
      let $filmes := doc("filmes.xml")//filme[@id=$sessao/filme]
      return 
          for $filme in $filmes
            return <tr>
                    
                    <td>
                         <titulo>{data($filme/@titulo)}</titulo>
                    </td>
                     <td>
                            {data($filme/genero)}
                      </td>
                      <td>
                            {data($filme/sinopse)}
                      </td>
                    </tr>
      }
      </table>
    
    
      
  </div>
</div>

</div>
</body>
</html>