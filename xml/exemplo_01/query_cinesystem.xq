declare namespace functx = "http://www.functx.com";
declare function functx:open-ref-document
  ( $refNode as node() )  as document-node() {

   if (base-uri($refNode))
   then doc(resolve-uri($refNode, base-uri($refNode)))
   else doc(resolve-uri($refNode))
 } ;
let $in-xml :=	
<a href="http://www.cinesystem.com.br/rio-anil/programacao">Some other file</a>
return


functx:open-ref-document(
  $in-xml/@href)

