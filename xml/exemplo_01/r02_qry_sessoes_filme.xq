xquery version "3.0";

declare namespace output = "http://www.w3.org/2010/xslt-xquery-serialization";

declare option output:method "html";
declare option output:media-type "text/html";

declare variable $s_filme  := "Lego";


<html>
<meta charset="UTF-8" />
<head>
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous" />

<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous" />

<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

</head>
<body>
<div class="container-fluid">
<h1>Sessoes do Filme</h1>
{
  for $filme in doc("filmes.xml")//filme
   let $sessoes := doc("sessoes.xml")/sessoes/sessao[filme=$filme/@id]
   (:let $horarios := $sessoes/horario:)
   where matches($filme/@titulo,$s_filme)
   order by $sessoes//sala and $sessoes//horario/@dia_sem and $sessoes//horario/@inicio
return 
  <div class="media">
    <div class="media-left media-middle">
      <a href="#">
        <img class="media-object" src="{data($filme/capa)}"/>
      </a>
    </div>
    <div class="media-body">
      <h4 class="media-heading">{data($filme/@titulo)}</h4>
      {data($filme/genero)}<br/>
      {data($filme/sinopse)}<br/>
      <ul class="list-group">
        <li class="list-group-item">Sessões</li>
        { for $sessao in $sessoes
          order by $sessao/sala
           return 
                <li class="list-group-item list-group-item-light">
                  Sala <sala>{data($sessao/sala)}</sala> - 
                  {data($sessao/horario/@dia_sem)} - {data($sessao/horario/@inicio)}
                </li>             
        }
        </ul>
         
    </div>
  </div>

}
</div>
</body>
</html>