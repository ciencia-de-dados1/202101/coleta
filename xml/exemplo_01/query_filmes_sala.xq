xquery version "1.0";

declare namespace output = "http://www.w3.org/2010/xslt-xquery-serialization";

declare option output:method "html";
declare option output:media-type "text/html";


<html>
<body>
<h1>Filmes e Salas</h1>
<ul>
{
for $x in doc("filmes.xml")/filmes/filme
order by $x/@titulo 
return <li>{concat($x/@id,'-',$x/@titulo,';',$x/duracao)}</li>
}
</ul>
</body>
</html>