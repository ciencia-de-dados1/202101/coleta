xquery version "3.0";

declare namespace output = "http://www.w3.org/2010/xslt-xquery-serialization";

declare option output:method "html";
declare option output:media-type "text/html";


<html>
<meta charset="UTF-8" />
<head>
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous" />

<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous" />

<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
</head>
<body>
<div class="container-fluid">
<h1>Filmes em cartaz</h1>
{
for $sessao in doc("sessoes.xml")/sessoes/sessao
let $horarios := $sessao/horarios
let $filme := doc("filmes.xml")/filmes/filme[@id=$sessao/filme]
order by $filme/@titulo
return 
  <div class="media">
    <div class="media-left media-middle">
      <a href="#">
        <img class="media-object" src="{data($filme/capa)}"/>
      </a>
    </div>
    <div class="media-body">
      <h4 class="media-heading">{data($filme/@titulo)}</h4>
      {data($filme/genero)}<br/>
      {data($filme/sinopse)}<br/>
      Sessões: {data($horarios/horario/@inicio)}<br/>
    </div>
  </div>
}
</div>
</body>
</html>