<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:template match="/">
  <html>
  <body>
  <h2>Reservas de laboratórios</h2>
  <xsl:apply-templates/>
  </body>
  </html>
</xsl:template>

<xsl:template match="reserva">
  <h3>Lab:
    <xsl:apply-templates select="@lab"/>
  </h3>
  <xsl:choose>
    <xsl:when test="requisitos/@tem > 0">
      <div id="requisitos">
        <ul>
        <xsl:for-each select="requisitos/software">
          <li><xsl:value-of select="@nome" /></li>
        </xsl:for-each>
        </ul>
      </div>
    </xsl:when>
    <xsl:otherwise>
     <span style="color:red">
     <b>Não tem requisitos</b>
     </span>
    </xsl:otherwise>

  </xsl:choose>
   <hr width="100%" align="center" />
</xsl:template>
</xsl:stylesheet>
